/*
 * main.c
 *
 *  Created on: May 26, 2016
 *      Author: chinesejie
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

//log
#include "slog/logger.h"

/*#define  DEBUG*/ //define in CFLAGS
/**
 * compiler built-in macro
 * windows：   _WIN32
 * mac os X：  __APPLE__
 * linux：     __linux__
 */
int main(int argc, char *argv[]) {
	// first parameter is path, second parameter is filename prefix
	// if path is null, use stdout to print log and ignore the second parameter
	if (log_init(NULL, NULL,
	LOGGER_ROTATE_BY_SIZE | LOGGER_ROTATE_PER_HOUR, 64) != 0) {
		printf("log_init error");
		exit(0);
	}

#if __APPLE__
	log_info("mac here");
#elif __linux__
	log_info("linux here");
#else
	log_fatal("only mac and linux are supported here");
	log_destroy();
	exit(0);
#endif
	//start... test vector

	log_destroy();
	return 0;
}

